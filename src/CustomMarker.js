import React, { Component, Fragment } from "react";
import { InfoWindow, Marker } from "react-google-maps";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/Table";
import moment from "moment";

export default class CustomMarker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      muncul: false,
      show: false,
      listOrder: [],
      temp:[],
      orderHariIni:[],
    };
  }

  onMarkerClick(data) {
    console.log("data");
    console.log("data with value: ", data);
    this.setState({
      muncul: true,
    });
  }

  onInfoWindowClose = () =>
    this.setState({
      muncul: false,
    });

  getDriverData = () => {};

  handleClose = () => {
    this.setState({
      show: false,
    });
  };

  detailDriver = () => {
    const { data } = this.props;
    fetch(
      `http://103.253.115.73/db/moojolcustomer/getDriverDetail.php?driverid=${data.driverid}`
    )
      .then((response) => response.json())
      .then((listOrder) => {
        let now = moment().format("YYYY-MM-DD");
        let orderHariIni = [];
        console.log(listOrder)
        for (let x = 0; x < listOrder.data.length; x++) {
          if (listOrder.data[x].tglorder.substr(0, 10) === now) {
            orderHariIni.push(listOrder.data[x]);
          }
        }

        this.setState({
          show: true,
          listOrder: listOrder,
          temp:data,
          orderHariIni:orderHariIni
        });
      });
  };

  render() {
    const { data } = this.props;
    const { show, listOrder, temp, orderHariIni } = this.state;
    let now = moment().format("DD-MM-YYYY");
    return (
      <Fragment>
        <Marker

          onClick={() => this.onMarkerClick(data)}
          {...this.props}
        >
          {this.state.muncul === true ? (
            <InfoWindow onCloseClick={() => this.onInfoWindowClose()}>
              <Card style={{ width: "18rem" }}>
                <Card.Body>
                  <Card.Title>{data.namadriver}</Card.Title>
                  <Card.Text>Terakhir Update: {data.tanggal}</Card.Text>
                  <Card.Text>Status Driver: {data.status}</Card.Text>
                  <Button variant="primary" onClick={() => this.detailDriver()}>
                    Lihat Detail
                  </Button>
                </Card.Body>
              </Card>
            </InfoWindow>
          ) : (<div/>)}
        </Marker>
        <Modal size="lg" show={show} onHide={() => this.handleClose()}>
          <Modal.Header closeButton>
          <Modal.Title>{temp.namadriver} - {temp.status}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div class="row">
              <div class="col-sm-12 mb-4">
                Hari ini ({now}): <b>{orderHariIni.length || 0} Order</b>
              </div>
              <div class="col-sm-12 mb-4">
                <i>Noted: Maksimal 15 Terakhir Data yang ditampilkan</i>
              </div>
            </div>
            <Table responsive  style={{fontSize:12}}striped bordered hover size="lg">
              <thead>
                <tr>
                  <th>Order No</th>
                  <th>Tanggal Order</th>
                  <th>Asal</th>
                  <th>Tujuan</th>
                  <th>Jenis Order</th>
                  <th>Harga</th>
                  <th>Payment</th>
                  <th>Status Order</th>
                </tr>
              </thead>
              <tbody>
              {
                  listOrder.data? listOrder.data.map((data, index) => {
                    return (
                      <tr>
                        <td>{data.orderno}</td>
                        <td>{data.tglorder}</td>
                        <td>{data.asaltext}</td>
                        <td>{data.tujuantext}</td>
                        <td>{data.jenis}</td>
                        <td>{data.harga}</td>
                        <td>{data.payment}</td>
                        <td>{data.jenisorder}</td>
                      </tr>
                    );
                  })
               :(<tr></tr>)}
              </tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => this.handleClose()}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </Fragment>
    );
  }
}
