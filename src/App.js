import React, { Component } from "react";
import "./App.css";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from "react-google-maps";
import { Card, Alert, Modal, Table, Button } from "react-bootstrap";
import CustomMarker from "./CustomMarker";
import Moment from "react-moment";
import moment from "moment";

const MapWithAMarker = withScriptjs(
  withGoogleMap((props) => (
    <GoogleMap defaultZoom={12} defaultCenter={{ lat: -6.2, lng: 106.816666 }}>
      {props.region.map((data, index) => {
        let tgl = data.tanggal.substr(0, 10);
        let now = moment().format("YYYY-MM-DD");
        if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="Selesai") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/kosong/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/kosong/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/kosong/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/kosong/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/kosong/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/kosong/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/kosong/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/kosong/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/kosong/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/kosong/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/kosong/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/kosong/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/kosong/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/kosong/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/kosong/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/kosong/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/kosong/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/kosong/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/kosong/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/kosong/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/kosong/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/kosong/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/kosong/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/kosong/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/kosong/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/kosong/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/kosong/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/kosong/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/kosong/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/kosong/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/kosong/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/kosong/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/kosong/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/kosong/350.png";
          } else {
            data.jenis = "/maps/kosong/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="Cancel") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/kosong/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/kosong/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/kosong/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/kosong/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/kosong/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/kosong/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/kosong/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/kosong/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/kosong/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/kosong/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/kosong/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/kosong/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/kosong/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/kosong/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/kosong/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/kosong/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/kosong/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/kosong/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/kosong/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/kosong/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/kosong/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/kosong/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/kosong/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/kosong/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/kosong/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/kosong/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/kosong/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/kosong/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/kosong/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/kosong/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/kosong/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/kosong/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/kosong/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/kosong/350.png";
          } else {
            data.jenis = "/maps/kosong/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="GetDriverMotor") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/ordermotor/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/ordermotor/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/ordermotor/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/ordermotor/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/ordermotor/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/ordermotor/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/ordermotor/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/ordermotor/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/ordermotor/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/ordermotor/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/ordermotor/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/ordermotor/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/ordermotor/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/ordermotor/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/ordermotor/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/ordermotor/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/ordermotor/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/ordermotor/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/ordermotor/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/ordermotor/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/ordermotor/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/ordermotor/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/ordermotor/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/ordermotor/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/ordermotor/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/ordermotor/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/ordermotor/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/ordermotor/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/ordermotor/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/ordermotor/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/ordermotor/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/ordermotor/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/ordermotor/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/ordermotor/350.png";
          } else {
            data.jenis = "/maps/ordermotor/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="TibaMotor") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/ordermotor/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/ordermotor/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/ordermotor/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/ordermotor/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/ordermotor/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/ordermotor/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/ordermotor/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/ordermotor/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/ordermotor/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/ordermotor/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/ordermotor/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/ordermotor/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/ordermotor/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/ordermotor/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/ordermotor/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/ordermotor/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/ordermotor/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/ordermotor/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/ordermotor/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/ordermotor/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/ordermotor/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/ordermotor/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/ordermotor/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/ordermotor/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/ordermotor/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/ordermotor/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/ordermotor/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/ordermotor/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/ordermotor/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/ordermotor/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/ordermotor/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/ordermotor/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/ordermotor/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/ordermotor/350.png";
          } else {
            data.jenis = "/maps/ordermotor/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="MulaiTestMotor") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/ordermotor/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/ordermotor/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/ordermotor/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/ordermotor/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/ordermotor/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/ordermotor/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/ordermotor/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/ordermotor/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/ordermotor/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/ordermotor/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/ordermotor/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/ordermotor/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/ordermotor/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/ordermotor/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/ordermotor/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/ordermotor/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/ordermotor/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/ordermotor/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/ordermotor/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/ordermotor/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/ordermotor/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/ordermotor/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/ordermotor/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/ordermotor/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/ordermotor/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/ordermotor/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/ordermotor/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/ordermotor/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/ordermotor/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/ordermotor/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/ordermotor/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/ordermotor/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/ordermotor/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/ordermotor/350.png";
          } else {
            data.jenis = "/maps/ordermotor/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="SampaiMotor") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/ordermotor/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/ordermotor/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/ordermotor/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/ordermotor/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/ordermotor/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/ordermotor/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/ordermotor/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/ordermotor/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/ordermotor/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/ordermotor/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/ordermotor/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/ordermotor/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/ordermotor/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/ordermotor/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/ordermotor/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/ordermotor/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/ordermotor/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/ordermotor/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/ordermotor/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/ordermotor/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/ordermotor/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/ordermotor/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/ordermotor/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/ordermotor/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/ordermotor/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/ordermotor/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/ordermotor/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/ordermotor/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/ordermotor/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/ordermotor/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/ordermotor/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/ordermotor/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/ordermotor/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/ordermotor/350.png";
          } else {
            data.jenis = "/maps/ordermotor/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="GetDriverKirim") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/paket/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/paket/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/paket/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/paket/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/paket/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/paket/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/paket/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/paket/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/paket/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/paket/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/paket/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/paket/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/paket/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/paket/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/paket/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/paket/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/paket/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/paket/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/paket/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/paket/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/paket/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/paket/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/paket/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/paket/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/paket/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/paket/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/paket/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/paket/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/paket/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/paket/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/paket/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/paket/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/paket/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/paket/350.png";
          } else {
            data.jenis = "/maps/paket/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="TibaKirim") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/paket/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/paket/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/paket/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/paket/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/paket/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/paket/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/paket/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/paket/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/paket/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/paket/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/paket/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/paket/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/paket/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/paket/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/paket/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/paket/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/paket/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/paket/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/paket/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/paket/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/paket/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/paket/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/paket/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/paket/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/paket/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/paket/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/paket/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/paket/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/paket/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/paket/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/paket/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/paket/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/paket/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/paket/350.png";
          } else {
            data.jenis = "/maps/paket/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="MulaiTestKirim") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/paket/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/paket/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/paket/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/paket/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/paket/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/paket/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/paket/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/paket/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/paket/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/paket/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/paket/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/paket/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/paket/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/paket/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/paket/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/paket/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/paket/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/paket/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/paket/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/paket/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/paket/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/paket/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/paket/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/paket/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/paket/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/paket/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/paket/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/paket/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/paket/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/paket/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/paket/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/paket/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/paket/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/paket/350.png";
          } else {
            data.jenis = "/maps/paket/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="SampaiKirim") {
          if (parseInt(data.heading) < 9) {
            data.jenis = "/maps/paket/0.png";
          } else if (parseInt(data.heading) < 19) {
            data.jenis = "/maps/paket/10.png";
          } else if (parseInt(data.heading) < 29) {
            data.jenis = "/maps/paket/20.png";
          } else if (parseInt(data.heading) < 39) {
            data.jenis = "/maps/paket/30.png";
          } else if (parseInt(data.heading) < 49) {
            data.jenis = "/maps/paket/40.png";
          } else if (parseInt(data.heading) < 59) {
            data.jenis = "/maps/paket/50.png";
          } else if (parseInt(data.heading) < 69) {
            data.jenis = "/maps/paket/60.png";
          } else if (parseInt(data.heading) < 79) {
            data.jenis = "/maps/paket/70.png";
          } else if (parseInt(data.heading) < 89) {
            data.jenis = "/maps/paket/80.png";
          } else if (parseInt(data.heading) < 99) {
            data.jenis = "/maps/paket/90.png";
          } else if (parseInt(data.heading) < 109) {
            data.jenis = "/maps/paket/100.png";
          } else if (parseInt(data.heading) < 119) {
            data.jenis = "/maps/paket/110.png";
          } else if (parseInt(data.heading) < 129) {
            data.jenis = "/maps/paket/120.png";
          } else if (parseInt(data.heading) < 139) {
            data.jenis = "/maps/paket/130.png";
          } else if (parseInt(data.heading) < 149) {
            data.jenis = "/maps/paket/140.png";
          } else if (parseInt(data.heading) < 159) {
            data.jenis = "/maps/paket/150.png";
          } else if (parseInt(data.heading) < 169) {
            data.jenis = "/maps/paket/160.png";
          } else if (parseInt(data.heading) < 179) {
            data.jenis = "/maps/paket/170.png";
          } else if (parseInt(data.heading) < 189) {
            data.jenis = "/maps/paket/180.png";
          } else if (parseInt(data.heading) < 199) {
            data.jenis = "/maps/paket/190.png";
          } else if (parseInt(data.heading) < 209) {
            data.jenis = "/maps/paket/200.png";
          } else if (parseInt(data.heading) < 219) {
            data.jenis = "/maps/paket/210.png";
          } else if (parseInt(data.heading) < 229) {
            data.jenis = "/maps/paket/220.png";
          } else if (parseInt(data.heading) < 249) {
            data.jenis = "/maps/paket/240.png";
          } else if (parseInt(data.heading) < 259) {
            data.jenis = "/maps/paket/250.png";
          } else if (parseInt(data.heading) < 269) {
            data.jenis = "/maps/paket/260.png";
          } else if (parseInt(data.heading) < 279) {
            data.jenis = "/maps/paket/270.png";
          } else if (parseInt(data.heading) < 289) {
            data.jenis = "/maps/paket/280.png";
          } else if (parseInt(data.heading) < 299) {
            data.jenis = "/maps/paket/290.png";
          } else if (parseInt(data.heading) < 309) {
            data.jenis = "/maps/paket/300.png";
          } else if (parseInt(data.heading) < 319) {
            data.jenis = "/maps/paket/310.png";
          } else if (parseInt(data.heading) < 329) {
            data.jenis = "/maps/paket/320.png";
          } else if (parseInt(data.heading) < 349) {
            data.jenis = "/maps/paket/340.png";
          } else if (parseInt(data.heading) < 359) {
            data.jenis = "/maps/paket/350.png";
          } else {
            data.jenis = "/maps/paket/0.png";
          }
        } else if (data.jeniskendaraan === "Motor" && data.status !== "On") {
          data.jenis = "/maps/off/off.png";
        } else {
          data.jenis = "";
        }
        return (
          <CustomMarker
            data={data}
            key={data.driverid}
            functions={props.functions}
            position={{
              lat: parseFloat(data.latitude),
              lng: parseFloat(data.longitude),
            }}
            icon={{
              url:`${data.jenis}`,
              scaledSize: new window.google.maps.Size(50, 50),
            }}
          />
        );
      })}
    </GoogleMap>
  ))
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region: [],
      temp:[],
      listOrder:[],
      selected: "",
      show:false,
      size:100,
      orderHariIni:[],
      kendaraan:'Motor',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    await this.getData();
    await setInterval(this.getData, 3000);
    await setInterval(this.deleteMarker(), 2000);
  }
  deleteMarker() {
    this.setState({
      region: [],
    });
  }

  getData = () => {
    this.getDriverLocation();
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.kendaraan !== this.state.kendaraan) {
      this.getData();
    }
    if (prevState.size !== this.state.size) {
      this.getData();
    }
  }

  getDriverLocation() {
    const { size,kendaraan } = this.state
    fetch(
      `http://103.253.115.73/db/moojolcustomer/getDriverLocationSkyDriver.php?size=${size}&kendaraan=${kendaraan}`
    )
      .then((response) => response.json())
      .then((region) => {
        this.setState({
          region: region.data,
        });
      });
  }

  handleClose = () => {
    this.setState({
      show: false,
    });
  };


  detailDriver = (data) => {
    fetch(
      `http://103.253.115.73/db/moojolcustomer/getDriverDetail.php?driverid=${data.driverid}`
    )
      .then((response) => response.json())
      .then((listOrder) => {
        let now = moment().format("YYYY-MM-DD");
        let orderHariIni = [];
        console.log(listOrder)
        for (let x = 0; x < listOrder.data.length; x++) {
          if (listOrder.data[x].tglorder.substr(0, 10) === now) {
            orderHariIni.push(listOrder.data[x]);
          }
        }

        this.setState({
          show: true,
          listOrder: listOrder,
          temp:data,
          orderHariIni:orderHariIni
        });
      });
  };

  handleChange(event) {
    this.setState({
      kendaraan: event.target.value,
    });
  }

  render() {
    const { show } = this.state;
    const { listOrder, temp, orderHariIni } = this.state
    let now = moment().format("DD-MM-YYYY");
    return (
      <div style={{ width: "100vw", height: "100vh" }}>
        <MapWithAMarker
          region={this.state.region}
          googleMapURL={`https://maps.googleapis.com/maps/api/js?key=AIzaSyD4DtpjgzXjI5q-ZjmDObB7r3Tw3Q7xl58&v=3.exp&libraries=geometry,drawing,places`}
          loadingElement={<div style={{ height: "100%" }} />}
          containerElement={<div style={{ height: "100%" }} />}
          mapElement={<div style={{ height: "100%" }} />}
        />
        <div
          style={{
            position: "absolute",
            top: 10,
            left: 200,
            padding: 20,
          }}
        >
          <Card body style={{ marginBottom: 10 }}>
            <b style={{marginBottom:50}}>PT MOOJOL PATRIOT INDONESIA</b> <br/>
            <div><b style={{color:'#f80000'}}>Merah</b>: Sedang Dalam Order Motor</div>
            <div><b style={{color:'#fe7200'}}>Orange</b>: Sedang Dalam Order Paket</div>
            <div><b style={{color:'#ff6f9a'}}>Merah Muda</b>: Sedang Kosong</div>
            <div><b style={{color:'#808080'}}>Abu Abu</b>: Sedang Off</div>
          </Card>
          {/* <Card body>Terdapat {this.state.dataOn.length} driver moojol status on</Card> */}
        </div>
        <div
          style={{
            position: "absolute",
            top: 10,
            right: 50,
            padding: 20,
          }}
        >
          <Card body>
            <div style={{ maxHeight: 700, overflow: "auto" }}>
              {this.state.region.map((data, index) => {
                if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="Selesai") {
                  return (
                    <div style={{backgroundColor: '#ff6f9a', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14 }}
                        >
                        {data.namadriver} - Motor
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="Cancel") {
                  return (
                    <div style={{backgroundColor: '#ff6f9a', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14}}
                        >
                        {data.namadriver} - Motor 
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="GetDriverMotor") {
                  return (
                    <div style={{backgroundColor: '#f80000', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Motor - Konfirmasi Approval Customer
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="TibaMotor") {
                  return (
                    <div style={{backgroundColor: '#f80000', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Motor - Driver Sedang Menuju Customer
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="MulaiTestMotor") {
                  return (
                    <div style={{backgroundColor: '#f80000', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Motor - Driver Sedang Mulai Perjalanan
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="SampaiMotor") {
                  return (
                    <div style={{backgroundColor: '#f80000', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Motor - Driver Sampai Tujuan
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="GetDriverKirim") {
                  return (
                    <div style={{backgroundColor: '#fe7200', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Paket - Konfirmasi Approval Customer
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="TibaKirim") {
                  return (
                    <div style={{backgroundColor: '#fe7200', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Paket - Driver Mengambil Barang
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="MulaiTestKirim") {
                  return (
                    <div style={{backgroundColor: '#fe7200', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Paket - Driver Mengantar Barang
                      </div>
                    </div>
                  );
                } else if (data.jeniskendaraan === "Motor" && data.status === "On" && data.jenisorder==="SampaiKirim") {
                  return (
                    <div style={{backgroundColor: '#fe7200', padding:5, margin:4}}>
                        <div
                          className="pointer"
                          onClick={() =>
                            this.detailDriver(data)
                          }
                          style={{ fontSize: 14, color:'white' }}
                        >
                        {data.namadriver} - Paket - Driver Sampai Di Tujuan
                      </div>
                    </div>
                  );
                } else {
                }
              })}
            </div>
          </Card>
        </div>
        <Modal size="lg" show={show} onHide={() => this.handleClose()}>
          <Modal.Header closeButton>
            <Modal.Title>{temp.namadriver} - {temp.status}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div class="row">
              <div class="col-sm-12 mb-4">
                Hari ini ({now}): <b>{orderHariIni.length || 0} Order</b>
              </div>
              <div class="col-sm-12 mb-4">
                <i>Noted: Maksimal 15 Terakhir Data yang ditampilkan</i>
              </div>
            </div>
            <Table responsive style={{fontSize:12}} striped bordered hover size="lg">
              <thead style={{fontSize:12}}>
                <tr>
                  <th>Order No</th>
                  <th>Tanggal Order</th>
                  <th>Asal</th>
                  <th>Tujuan</th>
                  <th>Jenis Order</th>
                  <th>Harga</th>
                  <th>Payment</th>
                  <th>Status Order</th>
                </tr>
              </thead>
              <tbody style={{fontSize:12}}>
                {
                  listOrder.data? listOrder.data.map((data, index) => {
             
                    return (
                      <tr>
                        <td>{data.orderno}</td>
                        <td>{data.tglorder}</td>
                        <td>{data.asaltext}</td>
                        <td>{data.tujuantext}</td>
                        <td>{data.jenis}</td>
                        <td>{data.harga}</td>
                        <td>{data.payment}</td>
                        <td>{data.jenisorder}</td>
                      </tr>
                    );
                  })
               :(<tr></tr>)}
              </tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => this.handleClose()}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default App;
